# RayPlatformerMovement #

The RayPlatformerMovement Unity project provides robust retro movement for 2D platformer characters using Unity's 2D raycasts for collision detection.

Allows for movement that sticks to slopes while maintaining momentum.

Intended to work with levels made with Unity's Tilemaps.

![Alt text](https://bitbucket.org/Aulis/rayplatformermovement/raw/055efcf59ec4e1e14feaba20c9ad25d9efb4a32f/READMEImages/Example.png)

- - -

# About key scripts #

Here is a brief description of the key monobehaviour scripts and their editor properties. More documentation for non-monobehaviour scripts can be found in code.

## RayPhysics2D ##

RayPhysics2D is a custom 2D platformer physics object script based on Unity's 2D physics raycasts and 2D colliders.

![Alt text](https://bitbucket.org/Aulis/rayplatformermovement/raw/055efcf59ec4e1e14feaba20c9ad25d9efb4a32f/READMEImages/RayPhysics2D%20component.png)

![Alt text](https://bitbucket.org/Aulis/rayplatformermovement/raw/055efcf59ec4e1e14feaba20c9ad25d9efb4a32f/READMEImages/RayPhysics2D%20table.png)

## RayPlatformerController ##

RayPlatformerController uses RayPhysics2D to create platformer character type movement, like walking, running, jumping and crouching.

Can be used with RayPlatformerInput or by scripts via properties like: ControlAxis, ControlJump, ControlRun and ControlCrouch.

![Alt text](https://bitbucket.org/Aulis/rayplatformermovement/raw/055efcf59ec4e1e14feaba20c9ad25d9efb4a32f/READMEImages/RayPlatformerController%20component.png)

![Alt text](https://bitbucket.org/Aulis/rayplatformermovement/raw/055efcf59ec4e1e14feaba20c9ad25d9efb4a32f/READMEImages/RayPlatformerController%20table.png)

## RayPlatformerInput ##

RayPlatformerInput is used to control RayPlatformerController. There are two input modes: keyboard and Unity's input system.

![Alt text](https://bitbucket.org/Aulis/rayplatformermovement/raw/055efcf59ec4e1e14feaba20c9ad25d9efb4a32f/READMEImages/RayPlatformerInput%20component.png)

![Alt text](https://bitbucket.org/Aulis/rayplatformermovement/raw/055efcf59ec4e1e14feaba20c9ad25d9efb4a32f/READMEImages/RayPlatformerInput%20table.png)

- - -

# Installation #

Project has been developed in Unity 2022.3.34. Functionality in newer versions is likely, but not quaranteed.

* Use the included Unity package file to import the scripts and examples to your project.
* In Unity choose 'Assets' -> 'Import package' -> 'Custom package'. Browse and select the RayPlatformerMovement_2020-xx-xx.unitypackage file.
* The Examples folder includes an example Tilemap level with a movable player.

- - -

# Usage #

* Create a tile palette and give each tile a physics shape (or use the palette provided in the examples)
* Add a Tilemap to your scene and create a test environment. Add TilemapCollider2D and CompositeCollider components to it.
* Add RayPhysics2D, RayPlatformerController and RayPlatformerInput components to your player gameobject.
* Configure the RayPhysics2D component's body size, body offset and tile size fields. Enable debug to help visualize your settings.
* Set the 'solid layers' field of your Rayphysics2D component to your Tilemap's layer.
* Set your RayPhysics2D component's OnCeilingCollision event to call RayPlatformerController's OnCeilingCollision method.
* Set the RayPhysics2D reference in your RayPlatformerController component.
* Set the RayPlatformerController reference in your RayPlatformerInput component.
* Play with the settings to get a good feel for the movement. See the provided example for reference.

- - -

# License #

MIT License

Copyright (c) 2020 Petteri Saarela

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.