﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

//RayPhysics2D is a custom physics object script based on Unity's 2D physics raycasts.
//Can be used to create more robust retro movement to platformer characters than with standard 2D physics.
//Allows for sticking to slopes and maintaining momentum.

namespace RayPlatformerMovement
{
    public class RayPhysics2D : MonoBehaviour
    {

        //Clearance values for raycast offsets at the edges
        private const float vSensorEdgeClearance = 0.025f;
        private const float hSensorEdgeClearanceTop = 0.025f;
        private const float hSensorEdgeClearanceBottom = 0.15f;//0.05f;
        private const float hSensorEdgeFactor = 0.5f;

        //Movement related values
        [Header("Movement")]
        private Vector2 velocity = Vector2.zero;
        public float groundSpeed = 0f;
        public float friction = 0.5f;
        public float bouncyness = 0.2f;
        public float slopeSlideFactor = 0.125f; //How much gravity affects sliding down slopes
        public bool gravityEnabled = true;
        public float gravity = 9.8f;
        public float velocityLimit = 0f;

        //Body related values
        [Header("Body")]
        public Vector2 bodySize = Vector2.zero;
        public Vector2 bodyOffset = Vector2.zero;
        public float tileSize = 1f;
        private float halfWidth = 0f;
        private float halfHeight = 0f;

        //Ground related values
        private bool onGround = false;
        private float groundAngle = 0f;
        private Vector2 groundTangent = Vector2.zero;
        private Vector2 groundNormal = Vector2.zero;

        //Sensor related values
        private Vector2 vSensorSize = Vector2.zero;
        private Vector2 hSensorSize = Vector2.zero;
        private float vSensorLength = 0f;
        private float hSensorLength = 0f;

        private Vector2[] floorSensorOffsets;
        private Vector2[] ceilingSensorOffsets;
        private Vector2[] rWallSensorOffsets;
        private Vector2[] lWallSensorOffsets;

        //Collision related values
        [Header("Collision")]
        public LayerMask solidLayers;
        public LayerMask leftSolidLayers;
        public LayerMask rightSolidLayers;
        public LayerMask upSolidLayers;
        public LayerMask downSolidLayers;
        private int collisionIterations = 1;
        private float maxDeltaMovement = 1f;

        [Header("Events")]
        public System.Action OnCeilingCollisionAction;
        public System.Action OnGroundCollisionAction;
        public UnityEvent OnCeilingCollision;
        public UnityEvent OnGroundCollision;

        //Debug draw the sensor raycasts
        [Header("Debug")]
        public bool drawDebug = false;
        public Color debugBodyColor = Color.red;
        public Color debugRayColor = Color.yellow;

        //----------------------------------------------
        //PROPERTIES
        //----------------------------------------------

        public float Speed { get { return velocity.magnitude; } }

        public float SqrSpeed { get { return velocity.sqrMagnitude; } }

        public Vector3 Velocity3D { get { return new Vector3(velocity.x, velocity.y, 0f); } }

        public bool Stopped { get { return velocity.sqrMagnitude == 0f; } }

        public Vector3 BodySize3D { get { return new Vector3(bodySize.x, bodySize.y, 0f); } }

        public Vector3 BodyOffset3D { get { return new Vector3(bodyOffset.x, bodyOffset.y, 0f); } }

        public float BodyHalfHeight { get { return halfHeight; } }

        public float BodyHalfWidth { get { return halfWidth; } }

        public Vector2 Velocity
        {
            get { return velocity; }
            set
            {
                if (value.y > velocity.y)
                    onGround = false;
                velocity = value;
            }
        }

        public bool OnGround
        {
            get { return onGround; }
            set { onGround = value; }
        }

        //----------------------------------------------
        //MONOBEHAVIOUR METHODS
        //----------------------------------------------

        private void OnValidate()
        {
            if (friction < 0f)
                friction = 0f;

            if (bouncyness < 0f)
                bouncyness = 0f;

            if (slopeSlideFactor < 0f)
                slopeSlideFactor = 0f;

            if (velocityLimit < 0f)
                velocityLimit = 0f;

            if (bodySize.x < 0f)
                bodySize.x = 0f;

            if (bodySize.y < 0f)
                bodySize.y = 0f;

            if (tileSize < 0.1f)
                tileSize = 0.1f;
        }

        //Value initializations
        void Awake()
        {
            BuildBody();
        }

        //Update physics independent from framerate
        void FixedUpdate()
        {
            //Calculate groundSpeed
            UpdateSlopeVelocity();
            UpdateFriction();
            LimitGroundSpeed();

            //Calculate axial velocities
            UpdateVelocity();
            UpdateGravity();
            LimitVelocity();

            //Iterate position and collision checks
            collisionIterations = Mathf.CeilToInt((velocity.magnitude * Time.deltaTime) / maxDeltaMovement);
            for (int i = 0; i < collisionIterations; i++)
            {
                UpdatePosition();

                //Collision checking
                //If we're going faster horizontally, then check horizontal collisions first
                if (Mathf.Abs(velocity.x) > Mathf.Abs(velocity.y))
                {
                    CheckHorizontalCollisions();
                    CheckCeilingCollisions();
                    CheckGroundCollisions();
                }
                //Else check vertical collisions first
                else
                {
                    CheckCeilingCollisions();
                    CheckGroundCollisions();
                    CheckHorizontalCollisions();
                }
            }
        }

        //----------------------------------------------
        //CONFIGURATION METHODS
        //----------------------------------------------

        //Initializing sensors and other values based on body and tile size
        public void BuildBody()
        {
            halfWidth = bodySize.x * 0.5f;
            halfHeight = bodySize.y * 0.5f;

            //Solving how many sensors are required for each axis and their lengths

            int bodyTileHeight = Mathf.CeilToInt(bodySize.y / tileSize);
            int bodyTileWidth = Mathf.CeilToInt(bodySize.x / tileSize);

            int hSensorCount = bodyTileWidth + 1;
            int vSensorCount = bodyTileHeight + 1;

            vSensorLength = halfHeight + (bodySize.x * 1.25f);
            hSensorLength = halfWidth;

            floorSensorOffsets = new Vector2[hSensorCount];
            ceilingSensorOffsets = new Vector2[hSensorCount];
            rWallSensorOffsets = new Vector2[vSensorCount];
            lWallSensorOffsets = new Vector2[vSensorCount];

            //Configuring the sensor offset positions in relation to body

            float hStep = (bodySize.x - vSensorEdgeClearance - vSensorEdgeClearance) / bodyTileWidth;
            float vStep = (bodySize.y - hSensorEdgeClearanceTop - hSensorEdgeClearanceBottom) / bodyTileHeight;

            for (int i = 0; i < hSensorCount; i++)
            {
                //Sensors on the ends of the axis have a specified edge clearance
                if (i == 0)
                {
                    floorSensorOffsets[i] = new Vector2(-halfWidth + vSensorEdgeClearance, 0f) + bodyOffset;
                    ceilingSensorOffsets[i] = new Vector2(-halfWidth + vSensorEdgeClearance, 0f) + bodyOffset;
                }
                else if (i == hSensorCount - 1)
                {
                    floorSensorOffsets[i] = new Vector2(halfWidth - vSensorEdgeClearance, 0f) + bodyOffset;
                    ceilingSensorOffsets[i] = new Vector2(halfWidth - vSensorEdgeClearance, 0f) + bodyOffset;
                }
                //Sensors in the middle are set uniformly
                else
                {
                    floorSensorOffsets[i] = new Vector2(-halfWidth + vSensorEdgeClearance + (i * hStep), 0f) + bodyOffset;
                    ceilingSensorOffsets[i] = new Vector2(-halfWidth + vSensorEdgeClearance + (i * hStep), 0f) + bodyOffset;
                }
            }

            for (int i = 0; i < vSensorCount; i++)
            {
                //Sensors on the ends of the axis have a specified edge clearance
                if (i == 0)
                {
                    lWallSensorOffsets[i] = new Vector2(0f, -halfHeight + hSensorEdgeClearanceBottom) + bodyOffset;
                    rWallSensorOffsets[i] = new Vector2(0f, -halfHeight + hSensorEdgeClearanceBottom) + bodyOffset;
                }
                else if (i == vSensorCount - 1)
                {
                    lWallSensorOffsets[i] = new Vector2(0f, halfHeight - hSensorEdgeClearanceTop) + bodyOffset;
                    rWallSensorOffsets[i] = new Vector2(0f, halfHeight - hSensorEdgeClearanceTop) + bodyOffset;
                }
                //Sensors in the middle are set uniformly
                else
                {
                    lWallSensorOffsets[i] = new Vector2(0f, -halfHeight + hSensorEdgeClearanceBottom + (i * vStep)) + bodyOffset;
                    rWallSensorOffsets[i] = new Vector2(0f, -halfHeight + hSensorEdgeClearanceBottom + (i * vStep)) + bodyOffset;
                }
            }

            maxDeltaMovement = Mathf.Min(halfWidth, halfHeight);
        }

        //Body scaling
        public void ConfigureBody(Vector2 scale)
        {
            ConfigureBody(bodySize * scale, bodyOffset * scale);
        }

        //Changing body size and offset
        public void ConfigureBody(Vector2 size, Vector2 offset)
        {
            float oldGroundDistance = -bodyOffset.y + halfHeight;

            bodySize = size;
            bodyOffset = offset;

            BuildBody();

            //Fix atop ground
            if (onGround == true)
            {
                float newGroundDistance = -bodyOffset.y + halfHeight;
                Vector3 position = transform.position;
                position.y += newGroundDistance - oldGroundDistance;
                transform.position = position;
            }
        }

        //----------------------------------------------
        //GROUND SPEED CALCULATION
        //----------------------------------------------

        //Downhill sliding
        void UpdateSlopeVelocity()
        {
            if (onGround == true && groundAngle != 0f)
            {
                groundSpeed -= slopeSlideFactor * gravity * Time.deltaTime * Mathf.Sin(Mathf.Deg2Rad * groundAngle);
            }
        }

        //Adds friction caused by the floor
        void UpdateFriction()
        {
            if (groundSpeed > 0f)
            {
                groundSpeed -= friction * Time.deltaTime;
                if (groundSpeed < 0f)
                    groundSpeed = 0f;
            }
            else
            {
                groundSpeed += friction * Time.deltaTime;
                if (groundSpeed > 0f)
                    groundSpeed = 0f;
            }
        }

        //Limits the range of ground speed so we don't miss tiles. Velocity limit is custom set here.
        void LimitGroundSpeed()
        {
            if (velocityLimit > 0f)
                groundSpeed = Mathf.Clamp(groundSpeed, -velocityLimit, velocityLimit);
        }

        //----------------------------------------------
        //VELOCITY CALCULATION
        //----------------------------------------------

        //Calculates velocity from ground speed and ground angle while ON ground
        void UpdateVelocity()
        {
            if (onGround == true)
            {
                velocity.x = groundSpeed * Mathf.Cos(Mathf.Deg2Rad * groundAngle);
                velocity.y = groundSpeed * Mathf.Sin(Mathf.Deg2Rad * groundAngle);
            }
        }

        //Adds gravity while in the air
        void UpdateGravity()
        {
            if (gravityEnabled == true && onGround == false)
                velocity.y -= gravity * Time.deltaTime;
        }

        //Limits the range of velocity by axis, so we don't miss tiles. Velocity limit is custom set
        void LimitVelocity()
        {
            if (velocityLimit > 0f)
            {
                velocity.x = Mathf.Clamp(velocity.x, -velocityLimit, velocityLimit);
                velocity.y = Mathf.Clamp(velocity.y, -velocityLimit, velocityLimit);
            }
        }

        //----------------------------------------------
        //UPDATE POSITION
        //----------------------------------------------

        //Updates position with a distance traveled with VELOCITY during the TIME of a single collision iteration
        void UpdatePosition()
        {
            transform.position += new Vector3(velocity.x * Time.deltaTime * (1f / collisionIterations), velocity.y * Time.deltaTime * (1f / collisionIterations), transform.position.z);
        }

        //----------------------------------------------
        //COLLISION DETECTION
        //----------------------------------------------

        //Check for ground collisions
        void CheckGroundCollisions()
        {
            RaycastHit2D[] rayHits = new RaycastHit2D[floorSensorOffsets.Length];
            int closestHitIndex = -1;
            float minDeltaY = float.MaxValue;

            LayerMask layerMask = solidLayers | upSolidLayers;

            //Shoot all downward facing raycasts and find the closest hit

            for (int i = 0; i < floorSensorOffsets.Length; i++)
            {
                rayHits[i] = Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y) + floorSensorOffsets[i], Vector2.down, vSensorLength, layerMask.value);

                if (rayHits[i].collider != null && rayHits[i].normal.y > 0f)
                {
                    float deltaY = transform.position.y + bodyOffset.y - rayHits[i].point.y;
                    if (deltaY < minDeltaY)
                    {
                        minDeltaY = deltaY;
                        closestHitIndex = i;
                    }
                }
            }

            //If atleast one raycast hit
            if (closestHitIndex >= 0)
            {
                float groundHeight = 0f;

                //Get the highest point detected & calculate ground tangent
                groundHeight = rayHits[closestHitIndex].point.y;
                groundNormal = rayHits[closestHitIndex].normal;
                groundTangent.x = rayHits[closestHitIndex].normal.y;
                groundTangent.y = -rayHits[closestHitIndex].normal.x;

                float groundDistanceFromFeet = transform.position.y - halfHeight + bodyOffset.y - groundHeight;
                bool firstTimeOnGround = false;

                //If we hit the ground
                if (groundDistanceFromFeet < 0f && onGround == false)
                {
                    if (velocity.y < 0f || groundNormal.y < 1f)
                    {
                        firstTimeOnGround = true;
                        onGround = true;
                        OnGroundCollisionAction?.Invoke();
                        OnGroundCollision.Invoke();
                    }
                }

                //Correct character position, update ground angle and calculate ground speed received when hitting slopes
                if (onGround == true)
                {
                    //Calculate ground angle
                    groundAngle = Vector2.Angle(Vector2.right, groundTangent);

                    Vector3 cross = Vector3.Cross(Vector3.right, new Vector3(groundTangent.x, groundTangent.y, 0f));
                    if (cross.z < 0f)
                        groundAngle = -groundAngle;

                    //Calculate ground speed received when hitting slopes. Ground speed depends on the angle of ground and axial velocities
                    if (firstTimeOnGround == true)
                    {
                        if (slopeSlideFactor > 0f)
                        {
                            if ((groundAngle >= 0f && groundAngle <= 22f) || (groundAngle <= 0f && groundAngle >= -22f))
                                groundSpeed = velocity.x;
                            else if ((groundAngle > 22f && groundAngle < 45f) || (groundAngle < -22f && groundAngle > -45f))
                            {
                                if (Mathf.Abs(velocity.x) > Mathf.Abs(velocity.y))
                                    groundSpeed = velocity.x;
                                else
                                {
                                    groundSpeed = velocity.y * 0.5f * Mathf.Sin(Mathf.Deg2Rad * groundAngle);
                                }
                            }
                            else if ((groundAngle >= 45f && groundAngle <= 90f) || (groundAngle <= -45f && groundAngle >= -90f))
                            {
                                if (Mathf.Abs(velocity.x) > Mathf.Abs(velocity.y))
                                    groundSpeed = velocity.x;
                                else
                                    groundSpeed = velocity.y * Mathf.Sin(Mathf.Deg2Rad * groundAngle);
                            }
                        }

                        //Handle vertical bouncing with surface normal
                        float bounceVelocity = -velocity.y * bouncyness;
                        if (bounceVelocity > 1f)
                        {
                            velocity.y = (rayHits[closestHitIndex].normal * bounceVelocity).y;
                            velocity.x += (rayHits[closestHitIndex].normal * bounceVelocity).x;
                            onGround = false;
                        }
                    }

                    //Set character atop ground
                    transform.position = new Vector3(transform.position.x, groundHeight + halfHeight - bodyOffset.y, transform.position.z);
                }
            }
            //If we didn't detect anything
            else
            {
                onGround = false;
                groundAngle = 0f;
                groundTangent = transform.right;
                groundNormal = transform.up;
            }
        }

        //Check for ceiling collisions
        void CheckCeilingCollisions()
        {
            RaycastHit2D[] rayHits = new RaycastHit2D[ceilingSensorOffsets.Length];

            float minDeltaY = float.MaxValue;
            int closestIndex = -1;

            LayerMask layerMask = solidLayers | downSolidLayers;

            //Shoot all upward facing raycasts and find the closest hit
            for (int i = 0; i < ceilingSensorOffsets.Length; i++)
            {
                rayHits[i] = Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y) + ceilingSensorOffsets[i], Vector2.up, vSensorLength, layerMask.value);

                if (rayHits[i].collider != null && rayHits[i].normal.y < 0f) //(rayHits[i].normal.y < 0f || rayHits[i].normal.y > 0f))
                {
                    float deltaY = rayHits[i].point.y - transform.position.y + bodyOffset.y;
                    if (deltaY < minDeltaY)
                    {
                        minDeltaY = deltaY;
                        closestIndex = i;
                    }
                }
            }

            //If atleast one raycast hit
            if (closestIndex >= 0)
            {
                //Get the lowest point in ceiling
                float ceilingHeight = 0f;
                ceilingHeight = rayHits[closestIndex].point.y;

                float ceilingDistanceFromHead = ceilingHeight - (transform.position.y + halfHeight + bodyOffset.y);

                //If we hit the ceiling
                if (ceilingDistanceFromHead < 0f)
                {
                    //Stop upwards motion
                    if (velocity.y > 0f)
                    {
                        velocity.y = 0f;
                        OnCeilingCollisionAction?.Invoke();
                        OnCeilingCollision.Invoke();
                    }

                    //Correct character's position
                    transform.position = new Vector3(transform.position.x, ceilingHeight - halfHeight - bodyOffset.y, transform.position.z);
                }
            }
        }

        //Check for wall collisions
        void CheckHorizontalCollisions()
        {
            RaycastHit2D[] rayHitsLeft = new RaycastHit2D[lWallSensorOffsets.Length];
            RaycastHit2D[] rayHitsRight = new RaycastHit2D[rWallSensorOffsets.Length];

            float minDeltaX = float.MaxValue;
            int closestIndex = -1;

            LayerMask leftLayerMask = solidLayers | rightSolidLayers;
            LayerMask rightLayerMask = solidLayers | leftSolidLayers;

            //Shoot all left facing raycasts and find the closest hit
            for (int i = 0; i < lWallSensorOffsets.Length; i++)
            {
                rayHitsLeft[i] = Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y) + lWallSensorOffsets[i], -Vector2.right, hSensorLength, leftLayerMask.value);

                if (rayHitsLeft[i].collider != null && rayHitsLeft[i].normal.y == 0f)
                {
                    float deltaX = transform.position.x + bodyOffset.x - rayHitsLeft[i].point.x;
                    if (deltaX < minDeltaX)
                    {
                        minDeltaX = deltaX;
                        closestIndex = i;
                    }
                }
            }

            //If atleast one raycast hit
            if (closestIndex >= 0)
            {
                float distanceFromWall = transform.position.x - halfWidth + bodyOffset.x - rayHitsLeft[closestIndex].point.x;

                //If we hit a wall then stop motion
                if (distanceFromWall < 0f)
                {
                    if (velocity.x < 0f)
                        velocity.x = 0f;

                    if (onGround && groundNormal.y < 1f && velocity.y > 0f)
                        velocity.y = 0f;

                    groundSpeed = 0f;

                    //Correct character's position
                    transform.position = new Vector3(rayHitsLeft[closestIndex].point.x + halfWidth - bodyOffset.x, transform.position.y, transform.position.z);
                }
            }

            //Reset helper values
            minDeltaX = float.MaxValue;
            closestIndex = -1;

            //Shoot all right facing raycasts and find the closest hit
            for (int i = 0; i < rWallSensorOffsets.Length; i++)
            {
                rayHitsRight[i] = Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y) + rWallSensorOffsets[i], Vector2.right, hSensorLength, rightLayerMask.value);

                if (rayHitsRight[i].collider != null && rayHitsRight[i].normal.y == 0f)
                {
                    float deltaX = rayHitsRight[i].point.x - transform.position.x + bodyOffset.x;
                    if (deltaX < minDeltaX)
                    {
                        minDeltaX = deltaX;
                        closestIndex = i;
                    }
                }
            }

            //If atleast one raycast hit
            if (closestIndex >= 0)
            {
                float distanceFromWall = rayHitsRight[closestIndex].point.x - (transform.position.x + halfWidth + bodyOffset.x);

                //If we hit a wall then stop motion
                if (distanceFromWall < 0f)
                {
                    if (velocity.x > 0f)
                        velocity.x = 0f;

                    if (onGround && groundNormal.y < 1f && velocity.y > 0f)
                        velocity.y = 0f;

                    groundSpeed = 0f;

                    //Correct character's position
                    transform.position = new Vector3(rayHitsRight[closestIndex].point.x - halfWidth - bodyOffset.x, transform.position.y, transform.position.z);
                }
            }
        }

        //----------------------------------------------
        //DEBUG METHODS
        //----------------------------------------------

        void OnDrawGizmos()
        {
            if (drawDebug == true && floorSensorOffsets != null && ceilingSensorOffsets != null && lWallSensorOffsets != null && rWallSensorOffsets != null)
            {
                //Draw body
                Gizmos.color = debugBodyColor;
                Gizmos.DrawWireCube(transform.position + BodyOffset3D, BodySize3D);

                //Draw sensor lines
                Gizmos.color = debugRayColor;

                for (int i = 0; i < floorSensorOffsets.Length; i++)
                {
                    Gizmos.DrawRay(transform.position + new Vector3(floorSensorOffsets[i].x, floorSensorOffsets[i].y, 0f), Vector3.down * vSensorLength);
                    Gizmos.DrawRay(transform.position + new Vector3(ceilingSensorOffsets[i].x, ceilingSensorOffsets[i].y, 0f), Vector3.up * vSensorLength);
                }

                for (int i = 0; i < lWallSensorOffsets.Length; i++)
                {
                    Gizmos.DrawRay(transform.position + new Vector3(lWallSensorOffsets[i].x, lWallSensorOffsets[i].y, 0f), -Vector3.right * hSensorLength);
                    Gizmos.DrawRay(transform.position + new Vector3(rWallSensorOffsets[i].x, rWallSensorOffsets[i].y, 0f), Vector3.right * hSensorLength);
                }
            }
        }

        public void DebugPrintIterations()
        {
            Debug.Log("Iterations: " + collisionIterations);
        }
    }
}
