﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//RayPlatformerInput is used to control RayPlatformerController

namespace RayPlatformerMovement
{
    public class RayPlatformerInput : MonoBehaviour
    {
        public RayPlatformerController platformerController;

        //Choose between keyboard keys and Unity's input axis
        public ControlTypes controlType = ControlTypes.KEYS;

        //Keyboard keys
        [Header("Input keys")]
        public KeyCode moveRightKey = KeyCode.RightArrow;
        public KeyCode moveLeftKey = KeyCode.LeftArrow;
        public KeyCode runKey = KeyCode.S;
        public KeyCode jumpKey = KeyCode.D;
        public KeyCode crouchKey = KeyCode.DownArrow;

        //Input axis allows for more versatile configuration in Project Settings -> Input
        [Header("Input axis")]
        public string movementAxis = "";
        public string runButton = "";
        public string jumpButton = "";
        public string crouchButton = "";

        public enum ControlTypes
        {
            AXIS,
            KEYS
        }

        // Update is called once per frame
        void Update()
        {
            UpdateControls();
        }

        public void UpdateControls()
        {
            if (controlType == ControlTypes.AXIS)
            {
                if (string.IsNullOrEmpty(movementAxis) == false)
                    platformerController.ControlAxis = Input.GetAxis(movementAxis);

                if (string.IsNullOrEmpty(jumpButton) == false)
                    platformerController.ControlJump = Input.GetButton(jumpButton);

                if (string.IsNullOrEmpty(runButton) == false)
                    platformerController.ControlRun = Input.GetButton(runButton);

                if (string.IsNullOrEmpty(crouchButton) == false)
                    platformerController.ControlCrouch = Input.GetButton(crouchButton);
            }
            else
            {
                if (Input.GetKey(moveLeftKey) ^ Input.GetKey(moveRightKey))
                    platformerController.ControlAxis = Input.GetKey(moveLeftKey) ? -1f : 1f;
                else
                    platformerController.ControlAxis = 0f;

                platformerController.ControlJump = Input.GetKey(jumpKey);
                platformerController.ControlRun = Input.GetKey(runKey);
                platformerController.ControlCrouch = Input.GetKey(crouchKey);
            }
        }
    }
}
