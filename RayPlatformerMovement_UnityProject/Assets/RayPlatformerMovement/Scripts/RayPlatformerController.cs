﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

//RayPlatformerController uses RayPhysics2D to create platformer character type movement, like walking, running, jumping and crouching
//Can be used with RayPlatformerInput or by scripts via the properties

namespace RayPlatformerMovement
{
    public class RayPlatformerController : MonoBehaviour
    {

        public RayPhysics2D rayPhysics2D;

        public float crouchHeightScale = 0.5f;

        //Control values
        private float controlAxis = 0f;
        private bool controlJump = false;
        private bool controlRun = false;
        private bool controlCrouch = false;

        //Movement values
        [Header("Movement")]
        public float walkingSpeed = 10f;
        public float runningSpeed = 10f;
        public float acceleration = 1f;
        public float deceleration = 10f;

        //Jump values
        [Header("Jump")]
        public float initialJumpSpeed = 6f;
        public float extendJumpSpeed = 0.2f;
        public float jumpExtendDuration = 0.5f;
        public float airControlAmount = 0.5f;

        private float direction = 1f;
        private bool hasJumped = false;
        private bool extendJump = false;
        private float jumpExtendTimer = 0f;

        private bool crouched = false;
        private float crouchedHalfHeight = 0f;

        private Vector2 originalBodyOffset = Vector2.zero;
        private Vector2 originalBodySize = Vector2.zero;

        //----------------------------------------------
        //PROPERTIES
        //----------------------------------------------

        public float ControlAxis
        {
            get { return controlAxis; }
            set
            {
                controlAxis = value;
                direction = Mathf.Sign(controlAxis);
            }
        }

        public bool ControlJump
        {
            get { return controlJump; }
            set { controlJump = value; }
        }

        public bool ControlRun
        {
            get { return controlRun; }
            set { controlRun = value; }
        }

        public bool ControlCrouch
        {
            get { return controlCrouch; }
            set { controlCrouch = value; }
        }

        public bool Crouched { get { return crouched; } }

        public float Direction
        {
            get { return direction; }
        }

        //----------------------------------------------
        //MONOBEHAVIOUR METHODS
        //----------------------------------------------

        private void OnValidate()
        {
            if (walkingSpeed < 0f)
                walkingSpeed = 0f;

            if (runningSpeed < 0f)
                runningSpeed = 0f;

            if (acceleration < 0f)
                acceleration = 0f;

            if (deceleration < 0f)
                deceleration = 0f;

            if (initialJumpSpeed < 0f)
                initialJumpSpeed = 0f;

            if (extendJumpSpeed < 0f)
                extendJumpSpeed = 0f;

            if (jumpExtendDuration < 0f)
                jumpExtendDuration = 0f;

            airControlAmount = Mathf.Clamp(airControlAmount, 0f, 1f);
        }

        private void Start()
        {
            if (rayPhysics2D != null)
            {
                rayPhysics2D.OnCeilingCollisionAction += OnCeilingCollision;
                crouchedHalfHeight = rayPhysics2D.BodyHalfHeight * crouchHeightScale;
                originalBodyOffset = rayPhysics2D.bodyOffset;
                originalBodySize = rayPhysics2D.bodySize;
            }
        }

        //Update movement independent from framerate
        private void FixedUpdate()
        {
            UpdateMovement();
            UpdateJump();
            UpdateCrouch();
        }

        //----------------------------------------------
        //CONTROL METHODS
        //----------------------------------------------

        //Update walking and running
        private void UpdateMovement()
        {
            float currentSpeed = 0f;
            float targetSpeed = 0f;
            float deltaSpeed = 0f;
            float maxDelta = 0f;

            if (rayPhysics2D.OnGround)
                currentSpeed = rayPhysics2D.groundSpeed;
            else
                currentSpeed = rayPhysics2D.Velocity.x;

            //If we have horizontal input
            if ((controlAxis < 0f) || (controlAxis > 0f))
            {
                //Calculate target velocity for movement. This is affected by running and crouching.

                targetSpeed = controlAxis;

                if (controlRun)
                {
                    //Running on the ground
                    if (rayPhysics2D.OnGround)
                    {
                        if (controlCrouch == false)
                            targetSpeed *= runningSpeed;
                        else
                            targetSpeed *= walkingSpeed;
                    }
                    //Running in the air
                    else
                    {
                        //Prevent activating running in the air. Preserve speed if higher than walking speed
                        if ((Mathf.Sign(controlAxis) == Mathf.Sign(currentSpeed)) && (Mathf.Abs(currentSpeed) > walkingSpeed))
                            targetSpeed *= Mathf.Abs(currentSpeed);
                        else
                            targetSpeed *= walkingSpeed;
                    }
                }
                else
                    targetSpeed *= walkingSpeed;


                //Calculate how much the speed is allowed to change this frame

                //If we're on the ground we can accelerate and decelerate
                if (rayPhysics2D.OnGround)
                {
                    //If we're already moving in the control direction use acceleration value to increase speed, else decelerate first
                    if (Mathf.Sign(controlAxis) == Mathf.Sign(currentSpeed))
                        maxDelta = acceleration * Time.deltaTime;
                    else
                        maxDelta = deceleration * Time.deltaTime;
                }
                //If we're in the air, we have a more limited way to change direction
                else
                    maxDelta = acceleration * airControlAmount * Time.deltaTime;

                deltaSpeed = targetSpeed - currentSpeed;
                deltaSpeed = Mathf.Clamp(deltaSpeed, -maxDelta, maxDelta);


                //Apply the change in speed to either groundSpeed of velocity.x, depending if we're on the ground or not

                if (rayPhysics2D.OnGround)
                {
                    rayPhysics2D.groundSpeed += deltaSpeed;
                }
                else
                {
                    Vector2 velocity = rayPhysics2D.Velocity;
                    velocity.x += deltaSpeed * airControlAmount;
                    rayPhysics2D.Velocity = velocity;
                    rayPhysics2D.groundSpeed = rayPhysics2D.Velocity.x;
                }
            }
            //If we don't have input and we're on the ground, then decelerate
            else
            {
                if (rayPhysics2D.OnGround)
                {
                    targetSpeed = 0f;
                    maxDelta = deceleration * Time.deltaTime;

                    deltaSpeed = targetSpeed - currentSpeed;
                    deltaSpeed = Mathf.Clamp(deltaSpeed, -maxDelta, maxDelta);

                    rayPhysics2D.groundSpeed += deltaSpeed;
                }
            }
        }

        private void UpdateJump()
        {
            if (controlJump == false && rayPhysics2D.OnGround == true)
                hasJumped = false;

            if (controlJump == true)
            {
                Vector2 velocity = rayPhysics2D.Velocity;

                //Add initial jump speed. Moving up a slope adds to jump height
                if (rayPhysics2D.OnGround == true && hasJumped == false)
                {
                    float jumpSpeed = initialJumpSpeed;

                    //If we are moving down a slope we need to reset vertical speed
                    if (velocity.y < 0f)
                        velocity.y = 0f;

                    velocity.y += jumpSpeed;

                    hasJumped = true;
                    extendJump = true;
                }

                //For as long as jump is held extend the jump
                if (extendJump == true)
                {
                    if (jumpExtendTimer < jumpExtendDuration)
                    {
                        jumpExtendTimer += Time.deltaTime;
                        velocity.y += extendJumpSpeed;
                    }
                    else
                    {
                        extendJump = false;
                        jumpExtendTimer = 0f;
                    }
                }

                rayPhysics2D.Velocity = velocity;
            }
            else
            {
                extendJump = false;
                jumpExtendTimer = 0f;
            }
        }

        private void UpdateCrouch()
        {
            if (controlCrouch)
            {
                //Configure body shorter and fix offset accordingly
                if (crouched == false)
                {
                    crouched = true;
                    float crouchedYOffset = rayPhysics2D.bodyOffset.y - (rayPhysics2D.BodyHalfHeight - crouchedHalfHeight);
                    rayPhysics2D.ConfigureBody(originalBodySize * new Vector2(1f, crouchHeightScale), new Vector2(originalBodyOffset.x, crouchedYOffset));
                }
            }
            else
            {
                //Configure body shorter and fix offset accordingly
                if (crouched == true)
                {
                    crouched = false;
                    rayPhysics2D.ConfigureBody(originalBodySize, originalBodyOffset);
                }
            }
        }

        //If we hit the ceiling we have to cancel jump extension
        private void OnCeilingCollision()
        {
            extendJump = false;
            jumpExtendTimer = 0f;
        }
    }
}
