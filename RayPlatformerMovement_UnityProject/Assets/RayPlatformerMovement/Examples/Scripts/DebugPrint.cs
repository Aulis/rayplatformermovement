﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RayPlatformerMovement
{
    public class DebugPrint : MonoBehaviour
    {
        public string prefix = "";
        public string text = "";

        public void Print()
        {
            Debug.Log(prefix + text);
        }

        public void Print(string text)
        {
            Debug.Log(prefix + text);
        }
    }
}