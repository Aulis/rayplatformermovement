﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RayPlatformerMovement
{
    public class ExampleAnimatorControl : MonoBehaviour
    {
        public Animator animator;
        public RayPlatformerController platformerController;

        // Update is called once per frame
        void Update()
        {
            animator.SetBool("crouch", platformerController.Crouched);
        }
    }
}