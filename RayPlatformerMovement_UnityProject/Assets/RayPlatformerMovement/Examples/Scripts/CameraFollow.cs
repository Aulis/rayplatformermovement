﻿using UnityEngine;
using System.Collections;

namespace RayPlatformerMovement
{
    public class CameraFollow : MonoBehaviour
    {

        public GameObject target;   //seurattava kohde
        public Rect limits;         //kameran liikkumisrajat


        // Update is called once per frame
        void FixedUpdate()
        {

            if (target != null)
            {
                //haetaan targeting positio
                Vector3 cameraPos = transform.position;
                cameraPos.x = target.transform.position.x;
                cameraPos.y = target.transform.position.y;

                //asetetaan rajat
                cameraPos.x = Mathf.Clamp(cameraPos.x, limits.xMin, limits.xMax);
                cameraPos.y = Mathf.Clamp(cameraPos.y, limits.yMin, limits.yMax);

                //asetetaan positio takaisin cameraan
                transform.position = cameraPos;
            }
        }

        //debug functio
        void OnDrawGizmos()
        {
            Gizmos.DrawWireCube(limits.center, limits.size);
        }

    }
}